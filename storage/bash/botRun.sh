#!/bin/bash

ABSOLUTE_FILENAME=`readlink -e "$0"`
DIRECTORY=`dirname "$ABSOLUTE_FILENAME"`

if ! ps aux | grep [b]ot:run > /dev/null
then
    php $DIRECTORY/../../artisan bot:run
fi