<?php

namespace App\Http\Controllers\Api;

use App\Models\Quote\Quote;
use GuzzleHttp\Client;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Symfony\Component\DomCrawler\Crawler;

class BashController extends Controller
{
    public function index(): ?JsonResponse
    {
        $crawler = new Crawler();
        $crawler->addHtmlContent(
            app(Client::class)
                ->get(config('quotes.url'))
                ->getBody(),
            'UTF-8');
        $node = $crawler->filter('article.quote')->first();
        if (!Quote::whereQuoteId($node->attr('data-quote'))->exists()) {
            $quote = Quote::create([
                'quote_id' => $node->attr('data-quote'),
                'content'  => htmlentities($node->filter('div.quote__body')
                    ->html()),
            ]);
            return response()->json([
                'data' => $quote,
            ]);
        }
        return null;
    }
}
