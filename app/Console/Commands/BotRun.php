<?php

namespace App\Console\Commands;

use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Telegram\Bot\Laravel\Facades\Telegram;

class BotRun extends Command
{

    protected const URLS = [
        'geek-jokes.sameerkumar.website/api',
        'api.chucknorris.io/jokes/random',
        'uselessfacts.jsph.pl/random.json?language=en',
        'api.kanye.rest/',
        'rzhunemogu.ru/Rand.aspx?CType=5',
        'icanhazdadjoke.com/slack',
    ];
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bot:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        dd(str_replace('"', '', htmlspecialchars_decode($this->prepareData())));
        Telegram::sendMessage([
            'chat_id' => config('telegram.chat_id'),
            'text' => str_replace('"', '', htmlspecialchars_decode($this->prepareData()))
        ]);
    }

    protected function prepareData(): ?string
    {
        $index = array_rand(self::URLS);
        $client = new Client(['headers', ['Content-Type' => 'application/x-www-form-urlencoded; charset=UTF-8;']]);
        $data = iconv("windows-1251","utf-8",$client->get(self::URLS[$index])
            ->getBody()
            ->getContents());
        switch ($index){
            case 0:
                return $data;
                break;
            case 1:
                return json_decode($data)->value;
                break;
            case 2:
                return json_decode($data)->text;
                break;
            case 3:
                return json_decode($data)->quote . ' (Kanye West)';
                break;
            case 4:
                return (string)simplexml_load_string($data)->content;
                break;
            case 5:
                return json_decode($data)->attachments[0]->text;
                break;

        }
    }
}
