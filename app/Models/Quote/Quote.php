<?php

namespace App\Models\Quote;

use Illuminate\Database\Eloquent\Model;

class Quote extends Model
{
    protected $fillable = [
        'quote_id',
        'content'
    ];
}
